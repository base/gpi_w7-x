from xmlrpc.client import ServerProxy
import datetime


MIDDLE_SERVER_ADDR = 'http://0.0.0.0:50000'
SOFTWARE_T1 = True  # send a T1 trigger through software (don't wait for hardware trigger)
PRETRIGGER = 5 # seconds between T0 and T1 (for T1 timing if SOFTWARE_T1 or for post-shot actions if not SOFTWARE_T1)
SHUTTER_CHANGE = 1 # seconds for the shutter to finish opening/closing


middle = ServerProxy(MIDDLE_SERVER_ADDR, verbose=False, allow_none=True)


def puff(starts, duration=0.05):
    if type(starts) == str:
        starts = starts.split('#')[0]
        starts = [float(s) for s in starts.split(' ') if s != '']
    permissions = [0]*4
    startsfull = [0]*4
    for i, s in enumerate(starts):
        if s >= 30:
            raise Exception('Start times >=30 seconds cause triggering problems in next shot.')
        startsfull[i] = s+1 # need to shift by 1 s for GERI
        permissions[i] = 1
    try:
        settings = {'puff_1_permission': permissions[0],
                    'puff_2_permission': permissions[1],
                    'puff_3_permission': permissions[2],
                    'puff_4_permission': permissions[3],
                    'puff_1_start': startsfull[0],
                    'puff_2_start': startsfull[1],
                    'puff_3_start': startsfull[2],
                    'puff_4_start': startsfull[3],
                    'puff_1_duration': duration,
                    'puff_2_duration': duration,
                    'puff_3_duration': duration,
                    'puff_4_duration': duration,
                    'shutter_change_duration': SHUTTER_CHANGE,
                    'software_t1': SOFTWARE_T1,
                    'pretrigger': PRETRIGGER}
        result = middle.handleSetPuffs(settings)

        if result == 1:
            print(f'Set at {datetime.datetime.now().strftime("%H:%M:%S")}: {starts}')
        else:
            print('Not puffing')
    except Exception as e:
        print(e)

def toggleShutter():
    middle.handleToggleShutter()
    data = middle.getDataForGUI()
    setting = data['shutter_setting']
    status = data['shutter_sensor']
    print('open' if setting == 1 else 'closed', setting, status)
